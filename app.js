const notifications = [
    {message: 'Lorem', read: true},
    {message: 'Ipsum', read: true},
    {message: 'Dolor', read: true},
    {message: 'Sit', read: false},
    {message: 'Amet', read: true}
];

let newNotification = [];

function endshift(data) {
    const element = data.shift();
    newNotification.push({ ...element, read: false});
    if (data.length > 0) {
        endshift(data);
    }
}

endshift(notifications);
console.log(newNotification);
